public interface I {
    void met1();
}
class B implements I{
    private long t;

    B(long t) {
        this.t = t;
    }

    @Override
    public void met1() {
        System.out.println("met1");
}}
class D{

    public void met1(int i){
        System.out.println("met1"+i);
    }

}
class G{
    double a;
    public double met3(){
        return a;
    }
}

class F{
    public void n(String s){

    }
}
class E{
    D d=new D();
    public void met1(){
    System.out.println("met1");
    }
}
class C{
    public void metB(B b){
        System.out.println("metB");

    }
}
