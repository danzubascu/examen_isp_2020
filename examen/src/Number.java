
import javax.swing.*;
import java.io.*;
import java.util.*;
import java.awt.event.*;

public class Number extends JFrame {
    JTextArea zona2, zona1;
    JButton calculeaza;

    Number() {

        setTitle("NrAparitiiCaractere");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(450, 400);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 150;
        int height = 30;

        calculeaza = new JButton("Calculeaza");
        calculeaza.setBounds(10, 100, width, height);
        calculeaza.addActionListener(new HandleCalculeaza());

        zona1 = new JTextArea();
        zona1.setBounds(70, 50, 300, height);

        zona2 = new JTextArea();
        zona2.setBounds(70, 150, width, height);

        add(zona1);
        add(zona2);
        add(calculeaza);

    }

    class HandleCalculeaza implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String prop = Number.this.zona1.getText();
            int N=0;
            for(int i = 0; i < prop.length(); i++) {
                if(prop.charAt(i) != ' ')//am banuit ca spatiul nu e caracter.(sper)
                    N++;
            }
            Number.this.zona2.setText(Integer.toString(N)); //afisez numarul de caractere( dar nu se vede ca pun peste "A")
            //si apoi caracterul 'a' de N ori
            String s="";
            for(int i=0;i<N;i++) {
                s=s+"a";
                zona2.setText(s);
            }
        }
    }


    public static void main(String[] args) {
        new Number();
    }


}